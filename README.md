# README #

### What is this repository for? ###

Genesis History

This is a single text file, containing approximate
timeline and genealogy information, as can be understood
from literally interpreting the Bible, Book of Mormon,
Doctrine and Covenants, and Pearl of Great Price. Also,
some speculation from outside sources, as well as some
speculation from gut-feeling "reading between the lines"
in the scriptures are included.  References are included
where appropriate.

### Contribution guidelines ###

Please talk to the author before contributing anything.

### Who do I talk to? ###

* Steven Schmidt
* schmidticus@gmail.com